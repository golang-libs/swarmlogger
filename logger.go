package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var encoderConf = zapcore.EncoderConfig{
	TimeKey:        "timestamp",
	LevelKey:       "level",
	NameKey:        "logger",
	CallerKey:      "caller",
	MessageKey:     "message",
	StacktraceKey:  "stacktrace",
	LineEnding:     zapcore.DefaultLineEnding,
	EncodeLevel:    zapcore.LowercaseLevelEncoder,
	EncodeTime:     zapcore.ISO8601TimeEncoder,
	EncodeDuration: zapcore.SecondsDurationEncoder,
	EncodeCaller:   zapcore.ShortCallerEncoder,
}

func InitLogger(debug bool) (err error) {
	level := zap.InfoLevel
	if debug {
		level = zap.DebugLevel
	}

	var jsonConfig = zap.Config{
		Level:         zap.NewAtomicLevelAt(level),
		Development:   debug,
		DisableCaller: true,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    encoderConf,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	zap.AddStacktrace(zap.ErrorLevel)
	logger, err := jsonConfig.Build()
	if err != nil {
		return err
	}

	zap.ReplaceGlobals(logger)
	zap.RedirectStdLog(logger)
	return nil
}
